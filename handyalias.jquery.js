
/**
 *  $.strExtractParamValue jQuery extentions
 *
 *  parse string like param[value] extract param and value
 *  @params
 *    input - string to parse
 *
 *  @return
 *    object like { param : 'param', value : 'value } or false if input string has wrong format
 */
(function($){
  $.extractParamValue = function ( input ) {
  var output = { param : null, value : null }
  var b1 = input.indexOf('[');
  var b2 = input.lastIndexOf(']');
  if ( b1 == -1 || b2 == -1 ) {
    return false;
  } else {
    output.param = input.slice( 0 , b1 );
    output.value = input.slice( b1 + 1 , b2 );
    return (output.param.length > 0 && output.value.length > 0) ? output : false;
  }
}})(jQuery);



Drupal.behaviors.handyalias = function(context) {
  
  /**
   *  Vocabulary and term handlers
   */
  
  var isVocabularyHandyalias    = $("#taxonomy-form-vocabulary input#edit-handyalias-enable");
  var disableMessage  = Drupal.t('This checkbox is disabled because of Handy Alias vocabulary do not support this feature. Disable Handy Alias for this vocabulary to enable this option');
  
  var handyaliasHiddenOptions  = $('#taxonomy-form-vocabulary #edit-handyalias-hidden-options');
  
  var isTags          = $("#taxonomy-form-vocabulary input#edit-tags");
  var tagLabel        = $("#taxonomy-form-vocabulary #edit-tags-wrapper .description");
  
  var isMultiple      = $("#taxonomy-form-vocabulary input#edit-multiple");
  var multipleLabel   = $("#taxonomy-form-vocabulary #edit-multiple-wrapper .description");
  
  var theme_handyaliasMessage = function(msg) {
    return '<span class="handy-alias-message" style="display:block;color:#fbb952;">' + msg + '</span>';
  }
  
  function enableVocabularyHandyalias() {
    // Disable and uncheck Tags checkbox, attach help message
    isTags.attr("disabled","disabled");
    isTags.attr("checked", false);
    tagLabel.prepend(theme_handyaliasMessage(disableMessage));
    // Disable and uncheck Multiply select checkbox, attach help message
    isMultiple.attr("disabled","disabled");
    isMultiple.attr("checked", false);
    multipleLabel.prepend(theme_handyaliasMessage(disableMessage));
    // Show content type checkbox list
    handyaliasHiddenOptions.fadeIn();
  }
  
  function disableVocabularyHandyalias() {
    // Enable Tags checkbox
    isTags.removeAttr("disabled");
    tagLabel.find('.handy-alias-message').remove();
    // Enable Multiply select checkbox
    isMultiple.removeAttr("disabled");
    multipleLabel.find('.handy-alias-message').remove();
    // Show content type checkbox list
    handyaliasHiddenOptions.fadeOut();
  }
  
  if ( isVocabularyHandyalias.attr('checked') ) {
    enableVocabularyHandyalias();
  } else {
    disableVocabularyHandyalias();
  }
  
  // Enable handy checkbox click handler
  isVocabularyHandyalias.click(function() {
    // If Enable handy checkbox has been checked
    if ( isVocabularyHandyalias.attr('checked') ) {
      enableVocabularyHandyalias();
    // If Enable handy checkbox has been unchecked
    } else {
      disableVocabularyHandyalias();
    }
  });
  
  // Link handy alias content type checkboxes with core content type checkbox list
  // When clicking handy alias checkboxes, core checkboxes behave the same way
  $('#taxonomy-form-vocabulary input[name^="handyalias_nodes"]').click(function(){
    var name = $.extractParamValue( $(this).attr('name') );
    var checked = $(this).attr('checked') ? true : false;
    $('#taxonomy-form-vocabulary input[name="nodes[' + name.value + ']"]').attr('checked', checked);
  });
  
  
  
  /**
   *  Contenty type form handlers
   */
  
  var ctypeHandyaliasEnable = $('#node-type-form #edit-handyalias-enable');
  var ctypeHandyaliasAttachHTML = $('#edit-handyalias-attach-html');
  
  function enableCTypeHandyalias() {
    // Show ctype handy alias options
    ctypeHandyaliasAttachHTML.attr('disabled', false);
  }
  
  function disableCTypeHandyalias() {
    // Hide ctype handy alias options
    ctypeHandyaliasAttachHTML.attr('disabled', true);
    ctypeHandyaliasAttachHTML.attr('checked', false);
  }
  
  if ( ctypeHandyaliasEnable.attr('checked') ) {
    enableCTypeHandyalias();
  } else {
    disableCTypeHandyalias();
  }
  
  ctypeHandyaliasEnable.click(function(){
    if ( ctypeHandyaliasEnable.attr('checked') ) {
      enableCTypeHandyalias();
    } else {
      disableCTypeHandyalias();
    }
  });
  
  
}