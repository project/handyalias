<?php

/**
 * @file
 * handyalias helper functions
 */


// ======================================================= SQL AND MISC =======================================================

/**
 * SQL interaction function
 *
 * Provide SELECT, INSERT, DELETE sql quries
 *
 * @param $op
 *   Required operation^ select, update or insert
 * @param $oid
 *  Object id: node id, term id or vocab id
 * @param $type
 *  Object type.
 * @param $alias
 *  Alias to set. Applied only with update poeration
 */
function _handyalias_sql($op, $oid, $type, $alias = '') {
  switch ($op) {
    case 'select' :
      return db_result( db_query("SELECT alias FROM {handy_alias} WHERE oid = '%d' AND type = '%d'", $oid , $type) );
    case 'update' :
      $record['oid'] = $oid;
      $record['type'] = $type;
      $record['alias'] = $alias;
      drupal_write_record( 'handy_alias', $record );
      return;
    case 'delete' :
      db_query( "DELETE FROM {handy_alias} WHERE oid = '%d' AND type = '%d' LIMIT 1", $oid , $type );
  }
}



/**
 * ======================================================= SAVE, VALIDATE FUNCTIONS =======================================================
 */



/**
 * Insert, update or delete vocabularies Handy Alias settings and vocabulary alias
 *
 * @param $vocab
 *  Vacabulary object
 * @param $op
 *  Operation: insert, update or delere
 */
function _handyalias_save_vocabulary( &$vocab, $op ) {
  switch ($op) {
    case 'insert':
    case 'update':
      _handyalias_check_vocab_settings( &$vocab );
      // Extract vocab settings variable
      $vocab_settings = variable_get('handyalias_vocab_settings', array());
      // If handy alias mode enabled
      if ($vocab->handyalias_enable) {
        $vocab_settings[$vocab->vid] = array_filter($vocab->handyalias_nodes);
        _handyalias_sql('delete', $vocab->vid, HANDYALIAS_TYPE_VOCAB);
        _handyalias_sql('update', $vocab->vid, HANDYALIAS_TYPE_VOCAB, $vocab->handyalias_vocab_alias);
        // Check if alias has been changed
        _handyalias_update_aliases($vocab->handyalias_vocab_alias, $vocab->handyalias_vocab_alias_old);
      }
      else {
        _handyalias_sql('delete', $vocab->vid, HANDYALIAS_TYPE_VOCAB);
        unset($vocab_settings[$vocab->vid]);
      }
      variable_set('handyalias_vocab_settings', $vocab_settings);
      break;
    case 'delete':
      _handyalias_sql('delete', $vocab->vid, HANDYALIAS_TYPE_VOCAB);
      variable_del('handyalias_vocab_settings');
      break;
  }
}



/**
 * Validate vocabulary configuration
 *
 * If js was disabled user could save vocab with uncompartable settings.
 * This function check it and fix it if necessary
 */
function _handyalias_check_vocab_settings( &$vocab ) {
  if (( $vocab->tags == 1 || $vocab->multiple == 1) && $vocab->handyalias_enable == 1 ) {
    $vocab->handyalias_enable = 0;
    drupal_set_message(
      t('Unable to use handy alias when "Tags" or "Multiple select" options enabled. If you want to use handy alias for this vocabulary, go back and disable "Tags" and "Multiple select" checkboxes'),
      'error'
    );
    return FALSE;
  }
  return TRUE;
}



/**
 * Validate alias
 */
function _handyalias_valid_alias($alias) {
  return (bool) preg_match("/^[^:!?&#@\/]+$/", $alias);
}



/**
 * Build alias path chain for hierarchy term
 *
 * @param $tid
 *   top (right) term id
 *
 * @return
 *   alias chain separated by slashes. Like this animals/catlike/tiger
 */
function _handyalias_get_alias_path($tid) {
  $parent_terms = taxonomy_get_parents_all($tid);
  for ( $i = count($parent_terms)-1; $i >= 0; $i-- ) {
    $alias_path_chain = _handyalias_sql('select', $parent_terms[$i]->tid, HANDYALIAS_TYPE_TERM);
    $alias_path[] = $alias_path_chain ? $alias_path_chain : 'term-' . $parent_terms[$i]->tid;
  }
  return !empty($alias_path) ? implode('/', $alias_path) : '';
}



/**
 * Get free (not linked with Handy Alias vocabs) content type
 *
 * @param $vid
 *   vocabulary id
 *
 * @return
 *   array of available for usage contenty types for current vocabulary
 */
function _handyalias_get_free_ctypes($vid = NULL) {
  // Make array of unavailable content types
  $vocab_settings_all = variable_get('handyalias_vocab_settings', array());
  $reserved_ctypes = array();
  if ( is_array($vocab_settings_all) && !empty($vocab_settings_all) ) {
    foreach ( $vocab_settings_all as $vocab_id => $ctypes ) {
      if ( is_array($ctypes) && !empty($ctypes) && $vid != $vocab_id ) {
        foreach ( $ctypes as $ctype ) {
          $reserved_ctypes[] = $ctype;
        }
      }
    }
  }
  // Get array of all content types and remove unavailable ctypes from it
  $free_ctypes = array();
  $all_ctypes = node_get_types('names');
  if ( is_array($all_ctypes) && !empty($all_ctypes) ) {
    foreach ( $all_ctypes as $ctype => $title ) {
      if ( !in_array($ctype, $reserved_ctypes) )
        $free_ctypes[$ctype] = $title;
    }
  }
  return $free_ctypes;
}



/**
 * Get Handy Alias vocabulary id by content type
 *
 * @param $input_ctype
 *   content type machin name, like "page" or "story"
 *
 * @return
 *   vid of vocabulary assigned to content type
 */
function _handyalias_get_ctype_vocab($input_ctype) {
  // Make array of unavailable content types
  $vocab_settings_all = variable_get('handyalias_vocab_settings', array());
  $ctypes_to_vid = array();
  if ( is_array($vocab_settings_all) && !empty($vocab_settings_all) ) {
    foreach ( $vocab_settings_all as $vocab_id => $ctypes ) {
      if ( is_array($ctypes) && !empty($ctypes) ) {
        foreach ( $ctypes as $ctype ) {
          $ctypes_to_vid[$ctype] = $vocab_id;
        }
      }
    }
  }
  return isset($ctypes_to_vid[$input_ctype]) ? $ctypes_to_vid[$input_ctype] : FALSE;
}


/**
 * Set warning message with recomendation for aliases bulk update
 * if alias has been updated
 *
 * @param $old_alias
 *   previous alias
 *
 * @param $new_alias
 *   new alias
 */
function _handyalias_update_aliases($new_alias, $old_alias) {
  if ($old_alias != '' && $new_alias != $old_alias) {
    drupal_set_message(
      t(
        'You just has changed vocabulary alias. To update existing aliases up to new alias use "<a href="@url">Bulk update</a>" page of pathauto module',
        array( '@url' => url('admin/build/path/update_bulk') )
      ),
      'warning'
    );
  }
}