
-- SUMMARY --

Handy alias module provides easy way to attach aliases to vocabularies,
terms and nodes. Further these aliases can be used for building vocabulary
structure corresponding paths for taxonomy pages and nodes.

-- REQUIREMENTS --

None.


-- INSTALLATION --

* Install as usual, see http://drupal.org/node/70151 for further information.

-- CONTACT --

Current maintainers:
* Alex Antipin (Stutzer) - http://drupal.org/user/124447

This project has been sponsored by:
* labs 42 (http://www.labs42.com)
  Professional web-design company in Moscow, Russia that supplies services
  of web design and development, web application development, professional
  ecommerce online shopping systems, website hosting, software development
  and interface design. We love Drupal and use it as a platform.
  We know the answer!